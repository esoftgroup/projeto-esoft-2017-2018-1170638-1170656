public class EspecificarEquipamentoController
{
    private Empresa m_oEmpresa;
    private Equipamento m_oEquipamento;
    public EspecificarEquipamentoController(Empresa oEmpresa)
    {
        this.m_oEmpresa = oEmpresa;
    }
    
    public void novoEquipamento()
    {
        this.m_oEquipamento = this.m_oEmpresa.novoEquipamento();
    }
    
    public void setDados(String sDescricao,String sEndLogico,String sEndFisico,String sFicheiro)
    {
        this.m_oEquipamento.setDescricao(sDescricao);
        this.m_oEquipamento.setEnderecoLogico(sEndLogico);
        this.m_oEquipamento.setEnderecoFisico(sEndFisico);
        this.m_oEquipamento.setFicheiroConfiguracao(sFicheiro);
    }
    
    public boolean registaEquipamento()
    {
        return this.m_oEmpresa.registaEquipamento(this.m_oEquipamento);
    }

    public String getEquipamentoString()
    {
        return this.m_oEquipamento.toString();
    }

    public void validaEquipamento()
    {
        this.m_oEmpresa.validaEquipamento(this.m_oEquipamento);
    }
	/**
	 * 
	 * @param ald
	 * @param mov
	 */
	public void setAreaRestrita(int ald, int mov) {
		// TODO - implement EspecificarEquipamentoController.setAreaRestrita
		throw new UnsupportedOperationException();
	}

	public void registaEquipamento() {
		// TODO - implement EspecificarEquipamentoController.registaEquipamento
		throw new UnsupportedOperationException();
	}
    
}

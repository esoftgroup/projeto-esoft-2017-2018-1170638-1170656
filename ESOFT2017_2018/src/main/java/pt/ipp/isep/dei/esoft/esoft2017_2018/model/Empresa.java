﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

import java.util.ArrayList;
import java.util.List;

public class Empresa
{
    private final List<Equipamento> m_lstEquipamentos;
    private RegistoColaboradores registoColaboradores;
    private RegistoCartoes registoCartoes;
    private RegistoPerfis registoPerfis;
    private RegistoAreaRestrita registoAreaRestrita;
    private RegistoEquipamentos registoEquipamentos;
    private RegistoFabricantes registoFabricantes;
    private RegistoAcessos registoAcessos;
    private int delay;
    private int intervalo;
    

    public Empresa()
    {
        this.m_lstEquipamentos = new ArrayList<Equipamento>();
        
        fillInData();
    }
    
    private void fillInData()
    {
        // Dados de Teste
        //Preenche alguns Terminais
        for(Integer i=1;i<=4;i++)
            addEquipamento(new Equipamento("Equipamento " + i.toString(),"0"+i.toString(),"",""));
        
        //Preencher outros dados aqui
        
    }
    
    public Equipamento novoEquipamento()
    {
        return new Equipamento();
    }
    
    public boolean validaEquipamento(Equipamento e)
    {
        boolean bRet = false;
        if (e.valida())
        {
           // Escrever aqui o código de validação
        
           //
           bRet = true; 
        }
        return bRet;
    }
    
    public boolean registaEquipamento(Equipamento e)
    {
        if (this.validaEquipamento(e))
        {
           return addEquipamento(e);
        }
        return false;
    }
    
    private boolean addEquipamento(Equipamento e)
    {
        return m_lstEquipamentos.add(e);
    }
    
    
    
    public List<Equipamento> getListaEquipamentos()
    {
        return this.m_lstEquipamentos;
    }

    public Equipamento getEquipamento(String sEquipamento)
    {
        for(Equipamento term : this.m_lstEquipamentos)
        {
            if (term.isIdentifiableAs(sEquipamento))
            {
                return term;
            }
        }
        
        return null;
    }
	public void getRegistoFabricantes() {

	}

	/**
	 * 
	 * @param fabId
	 */
	public void getFabricante(int fabId) {
		// TODO - implement Empresa.getFabricante
		throw new UnsupportedOperationException();
	}

	public void getRegistoAreas() {
		// TODO - implement Empresa.getRegistoAreas
		throw new UnsupportedOperationException();
	}

	public void getRegistoEquipamentos() {
		// TODO - implement Empresa.getRegistoEquipamentos
		throw new UnsupportedOperationException();
	}

//////////////////////////////////////////////////////UC2 METODOS//////////////////////////////////////////////////////

	/**
	 * 
	 * @param Area
	 */
	public void validaAreas(int Area) {
		// TODO - implement Empresa.validaAreas
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param IntervaloTempo
	 */
	public void validaIntervalo(int IntervaloTempo) {
		// TODO - implement Empresa.validaIntervalo
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param IntervaloTempo
	 * @param Area
	 */
	public void ObterRegistos(int IntervaloTempo, int Area) {
		// TODO - implement Empresa.ObterRegistos
		throw new UnsupportedOperationException();
	}

	public void consultaAreas() {
		// TODO - implement Empresa.consultaAreas
		throw new UnsupportedOperationException();
	}

/////////////////////////////////////////////////////////UC3/////////////////////////////////////////////////////////

	public void getRegistoColaboradores() {

	}

	public void getRegistoPerfis() {
		// TODO - implement Empresa.getRegistoPerfis
		throw new UnsupportedOperationException();
	}

	public void getRegistoPerfis() {
		// TODO - implement Empresa.getRegistoPerfis
		throw new UnsupportedOperationException();
	}

	public void getRegistoEquipamentos() {
		// TODO - implement Empresa.getRegistoEquipamentos
		throw new UnsupportedOperationException();
	}

////////////////////////////////////////////////////////UC4/////////////////////////////////////////////////////////////

	public void getRegistoColab() {

	}

	public void obtemRegistoCartoes() {

	}

///////////////////////////////////////////////////UC10//////////////////////////////////////////////////////////////

	public void obtemRegistoCartoes() {
		// TODO - implement Empresa.obtemRegistoCartoes
		throw new UnsupportedOperationException();
	}

//////////////

public void getRegistoColaboradores() {
        // TODO - implement Empresa.getRegistoColaboradores
        throw new UnsupportedOperationException();
    }

    public void getRegistoCartoes() {
        // TODO - implement Empresa.getRegistoCartoes
        throw new UnsupportedOperationException();
    }

    public void getRegistoEquipamentos() {
        // TODO - implement Empresa.getRegistoEquipamentos
        throw new UnsupportedOperationException();
    }

    public void getRegistoAcessos() {
        // TODO - implement Empresa.getRegistoAcessos
        throw new UnsupportedOperationException();
    }

    public void getRegistoAreaRestrita() {
        // TODO - implement Empresa.getRegistoAreaRestrita
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param registoAreaRestrita
     */
    public void setRegistoAreaRestrita(RegistoAreaRestrita registoAreaRestrita) {
        this.registoAreaRestrita = registoAreaRestrita;
    }

    public void getRegistoPerfis() {
        // TODO - implement Empresa.getRegistoPerfis
        throw new UnsupportedOperationException();
    }

    /**
     *
     * @param registoPerfis
     */
    public void setRegistoPerfis(RegistoPerfis registoPerfis) {
        this.registoPerfis = registoPerfis;
    }
    public void getDelay() {
		// TODO - implement Empresa.getDelay
		throw new UnsupportedOperationException();
	}

	public void getIntervalo() {
		// TODO - implement Empresa.getIntervalo
		throw new UnsupportedOperationException();
	}
    
}
    
    

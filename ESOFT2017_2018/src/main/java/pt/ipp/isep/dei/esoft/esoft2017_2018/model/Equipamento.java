﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.esoft2017_2018.model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Equipamento
{
    private String m_sDescricao;
    private String m_sEnderecoLogico;
    private String m_sEnderecoFisico;
    private String m_sFicheiroConfiguracao;
    
    public Equipamento()
    {
    }
    
    public Equipamento(String sDescricao,String sEnderecoLogico,String sEnderecoFisico,String sFicheiroConfiguracao)
    {
        this.setDescricao(sDescricao);
        this.setEnderecoLogico(sEnderecoLogico);
        this.setEnderecoFisico(sEnderecoFisico);
        this.setFicheiroConfiguracao(sFicheiroConfiguracao);
    }
    
    public final void setDescricao(String sDescricao)
    {
        this.m_sDescricao = sDescricao;
    }
    
    public final void setEnderecoLogico(String sEnderecoLogico)
    {
        this.m_sEnderecoLogico = sEnderecoLogico;
    }
    
    public String getEnderecoLogico()
    {
        return this.m_sEnderecoLogico;
    }
    
    public final void setEnderecoFisico(String sEnderecoFisico)
    {
        this.m_sEnderecoFisico = sEnderecoFisico;
    }
    
    public final void setFicheiroConfiguracao(String sFicheiroConfiguracao)
    {
        this.m_sFicheiroConfiguracao = sFicheiroConfiguracao;
    }
    
    public boolean valida()
    {
        // Escrever aqui o código de validação
        
        //
        return true;
    }
    
    public boolean isIdentifiableAs(String sID)
    {
        return this.m_sEnderecoLogico.equals(sID);
    }
    
    @Override
    public String toString()
    {
        return this.m_sDescricao + ";" + this.m_sEnderecoLogico + ";...";
    }

	public void setDados(int dados) {
		// TODO - implement Equipamento.setDados
		throw new UnsupportedOperationException();
	}

	public void valida() {
		// TODO - implement Equipamento.valida
		throw new UnsupportedOperationException();
	}

	public void getAreaRestrita() {
		// TODO - implement Equipamento.getAreaRestrita
		throw new UnsupportedOperationException();
	}

	public void getMovimento() {
		// TODO - implement Equipamento.getMovimento
		throw new UnsupportedOperationException();
	}

	public void getModelo() {
		// TODO - implement Equipamento.getModelo
		throw new UnsupportedOperationException();
	}

	public void getPeriodoAutorizacao() {
		// TODO - implement Equipamento.getPeriodoAutorizacao
		throw new UnsupportedOperationException();
	}
}
